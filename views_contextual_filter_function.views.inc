<?php

/**
 * Implements hook_views_plugins().
 */
function views_contextual_filter_function_views_plugins() {
  return array(
    'argument default' => array(
      'views_contextual_filter_function' => array(
        'title' => t('Custom Function'),
        'handler' => 'views_plugin_argument_views_contextual_filter_function_list',
      ),
    ),
  );
}
