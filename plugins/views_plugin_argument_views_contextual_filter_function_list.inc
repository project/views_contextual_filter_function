<?php
/**
 * @file
 * Definition of Views Contextual Filter plugin to select a custom function.
 *
 * @todo Allow passing in parameters.
 */

/**
 * Default argument plugin to select from custom function list.
 */
class views_plugin_argument_views_contextual_filter_function_list extends views_plugin_argument_default {

  /**
   * Define the options form to pick function.
   */
  function options_form(&$form, &$form_state) {
    $declared = module_invoke_all('views_contextual_filter_function_info');
    // Adjust structure for admin setting options.
    $list = array();
    foreach ($declared as $func) {
      $list[$func->function] = $func->label;
    }
    $form['function'] = array(
      '#type' => 'select',
      '#title' => t('Choose custom function'),
      '#description' => t('Custom functions can be declared via the !func info hook.', array('!func' => 'views_contextual_filter_function_info')),
      '#options' => $list,
      '#default_value' => $this->options['function'],
    );
  }

  /**
   * Return a value to the contextual filter.
   */
  function get_argument() {
    $function_name = $this->options['function'];
    if (function_exists($function_name)) {
      return $function_name();
    }
    else {
      return FALSE;
    }
  }

  /**
   * Initialize this plugin with the view and the argument is is linked to.
   */
  function init(&$view, &$argument, $options) {
    parent::init($view, $argument, $options);
  }
}
