<?php
/**
 * @file
 * Documentation for the Views Contextual Filter Function module.
 */

/**
 * @defgroup views_contextual_filter_function hooks
 *
 * @{
 * Hooks that can be implemented to externally extend the
 * Views Contextual Filter Function module.
 */


/**
 * Implements hook_views_contextual_filter_function_info().
 *
 * Declare custom functions available via contextual filter admin settings.
 *
 * @return object
 *   Views admin label and name of function.
 */
function mymodule_views_contextual_filter_function_info() {
  return (object) array(
    'label' => 'Get some magical filter value',
    'function' => 'mymodule_get_value_somehow',
  );
}

/**
 * Provide custom value to views contextual filter.
 *
 * @return string
 */
function mymodule_get_value_somehow() {
  return 'something';
}


/**
 * @}
 * End hook documentation.
 */
